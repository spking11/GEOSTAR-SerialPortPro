## Gmail的应用程序登陆

- [如何生成应用专用密码](https://support.google.com/mail/answer/185833)（推荐）
- 在[这里](https://www.google.com/settings/security/lesssecureapps)启用**允许安全性较低的应用**

## QQ邮箱的授权码

- [什么是授权码，它又是如何设置？](https://service.mail.qq.com/cgi-bin/help?subtype=1&&id=28&&no=1001256)