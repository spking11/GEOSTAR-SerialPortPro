## Gmail的應用程序登入

- [如何生成應用專用密碼](https://support.google.com/mail/answer/185833)（推薦）
- 在[這裡](https://www.google.com/settings/security/lesssecureapps)啟用**允許安全性較低的應用**

## QQ郵箱的授權碼

- [什麼是授權碼，它又是如何設定？](https://service.mail.qq.com/cgi-bin/help？subtype=1&&id=28&&no=1001256)